user www-data;
pid /var/run/nginx.pid;
worker_processes auto;
worker_rlimit_nofile 32768;

events {
    # cat /proc/sys/fs/file-max
    # 524288
    worker_connections 2048;
    multi_accept on;
    use epoll;
}

http {
    server_tokens off;
    sendfile on;
    tcp_nopush on;
    tcp_nodelay on;
    default_type  application/octet-stream;
    access_log  /dev/stdout;
    error_log /dev/stderr;
    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';
    keepalive_timeout 620;
    keepalive_requests 10000;
    client_max_body_size 30m;
    send_timeout 130;
    client_body_timeout 130;
    client_header_timeout 130;
    proxy_send_timeout 130;
    proxy_read_timeout 130;
    reset_timedout_connection on;
    limit_conn_zone $binary_remote_addr zone=addr:5m;
    limit_conn addr 100;
    charset UTF-8;
    gzip on;
    gzip_http_version 1.0;
    gzip_disable "msie6";
    gzip_proxied any;
    gzip_min_length 1024;
    gzip_comp_level 6;
    gzip_types text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript application/javascript;
    open_file_cache max=100000 inactive=20s;
    open_file_cache_valid 30s;
    open_file_cache_min_uses 2;
    open_file_cache_errors on;
    include /etc/nginx/mime.types;
    include /etc/nginx/conf.d/*.conf;
}