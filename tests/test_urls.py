import pytest


@pytest.mark.django_db
class TestAcceptance:

    @pytest.mark.parametrize('url, status_code', [
        ('/admin/', 200),
        ('/admin/login/', 302),
        ('/admin/logout/', 200),
    ])
    def test_admin_views(self, admin_client, url, status_code):
        c = admin_client.get(url)
        assert c.status_code == status_code
