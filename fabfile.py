from invoke import task


@task
def collectstatic(ctx):
    ctx.run(
        'cd /var/www/django_web_boilerplate_vps && \
        sudo -u www-data /opt/py37/bin/python bin/manage.py \
        collectstatic --no-input'
    )


@task
def git_pull(ctx):
    ctx.run(
        'cd /var/www/django_web_boilerplate_vps && \
        sudo -u www-data git pull origin master'
    )


@task
def migrate(ctx):
    ctx.run(
        'cd /var/www/django_web_boilerplate_vps && \
        sudo -u www-data /opt/py37/bin/python bin/manage.py migrate'
    )


@task
def uwsgi_reload(ctx):
    ctx.run(
        'sudo -u www-data touch /var/www/django_web_boilerplate_vps/tmp/.reload'
    )
