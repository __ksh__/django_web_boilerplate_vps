from django.conf import settings
from factory import fuzzy, LazyAttribute
from factory.django import DjangoModelFactory
from plugins.app.models import Robot


# TODO: もうちょい書く？
class RobotFactory(DjangoModelFactory):
    class Meta:
        model = Robot

    name = fuzzy.FuzzyText(length=16, prefix='robot_')
    lang = fuzzy.FuzzyText(length=16, prefix='robot_lang_')
