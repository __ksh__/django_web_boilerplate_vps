from django.test import TestCase
from plugins.app.tests.factory_models import RobotFactory


# FIXME: もうちょいひねって
class RobotTestCase(TestCase):

    def test_robot_speak(self):
        robot = RobotFactory()
        self.assertEqual(robot.speak(), f"{robot.name} speaks {robot.lang}.")
